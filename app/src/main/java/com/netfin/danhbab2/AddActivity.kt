package com.netfin.danhbab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add.*

class AddActivity : AppCompatActivity() {
    lateinit var mySQLlite: SQLlite
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        mySQLlite = SQLlite(this)
        onClickSave()
    }

    private fun onClickSave() {
        btn_save.setOnClickListener {
            if (TextUtils.isEmpty(edt_name.text)){
                edt_name.error ="Enter name"
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(edt_numberphone.text)){
                edt_numberphone.error ="Enter name"
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(edt_sex.text)){
                edt_sex.error ="Enter name"
                return@setOnClickListener
            }
            val insert = mySQLlite.insertData(edt_name.text.toString(),edt_numberphone.text.toString(),edt_sex.text.toString())
            if (insert){
                Toast.makeText(this,"Done!",Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this,"Error!",Toast.LENGTH_SHORT).show()
            }
        }
    }
}
