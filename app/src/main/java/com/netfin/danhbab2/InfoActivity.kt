package com.netfin.danhbab2

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
    lateinit var mySQLlite: SQLlite
    var idInfo: String =""
    var nameInfo: String = ""
    var phoneInfo:String = ""
    var sexInfo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val idInfo = intent.getIntExtra("idInfo",-1)
        mySQLlite = SQLlite(this)
        getDataInfo(idInfo.toString())
        btn_delete_info.setOnClickListener {
            showDialogDelete(idInfo.toString())
        }
        btn_edit_info.setOnClickListener {
            callIntentUpdate(idInfo)
        }

    }

    override fun onStart() {
        super.onStart()
        getDataInfo(idInfo.toString())
    }
    fun showDialogDelete(id: String){
        val dialog = AlertDialog.Builder(this)
        val dialogView = dialog.create()
        dialog.setMessage("Bạn có thực sự muốn xóa?")
        val actionOK = {dialogaction: DialogInterface, which: Int ->
            deleteDataInfo(id)
            Toast.makeText(this,"Delete",Toast.LENGTH_SHORT).show()
            finish()
        }
        dialog.setPositiveButton("OK", DialogInterface.OnClickListener(function = actionOK))
        val actionCANCEL = {dialogaction: DialogInterface, which: Int ->
            dialogaction.dismiss()
        }
        dialog.setNegativeButton("CANCEL", DialogInterface.OnClickListener(function = actionCANCEL))

        dialog.show()

    }
    fun callIntentUpdate(id: Int){
        val intentUpdate = Intent(this,UpdateActivity::class.java)
        val idInfo = idInfo
        val name = nameInfo
        val numberphone = phoneInfo
        val sex = sexInfo
        val contactArray = arrayListOf(idInfo,name,numberphone,sex)
        intentUpdate.putExtra("idInfo",contactArray)
        startActivity(intentUpdate)
    }

    fun getDataInfo(id: String) {

        val res = mySQLlite.getData(id)
        println("----------------------------getdata: $res")
        if (res.moveToFirst()) {
            idInfo = res.getString(0)
            nameInfo = res.getString(1)
            phoneInfo = res.getString(2)
            sexInfo = res.getString(3)
            txt_name_info.text = "Name: "+res.getString(1)
            println("----------------------------getdata: ${res.getString(1)}")
            txt_numberphone_info.text = "Phone: "+res.getString(2)
            txt_sex_info.text = "Sex: "+res.getString(3)
        }
    }
    fun deleteDataInfo(id: String) {
        val deleterows = mySQLlite.daleteData(id)
    }
}
