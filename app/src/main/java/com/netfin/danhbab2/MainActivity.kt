package com.netfin.danhbab2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.cell_item.view.*

class MainActivity : AppCompatActivity() {
    var dataList: ArrayList<ModelData> = ArrayList()
    lateinit var mySQLlite: SQLlite
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mySQLlite = SQLlite(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter= ContactAdapter(dataList)
        onClickAdd()

    }

    override fun onStart() {
        super.onStart()
        viewAllData()
        recyclerView.adapter?.notifyDataSetChanged()
    }
    fun viewAllData() {
            dataList.clear()
            val res = mySQLlite.getAllData()
            if (res.getCount() == 0) {
                Toast.makeText(this,"No data!",Toast.LENGTH_SHORT).show()
            } else {
                while (res.moveToNext()) {
                    dataList.add(ModelData(res.getString(0).toInt(),res.getString(1),res.getString(2),res.getString(3)))
                }
            }
    }

    fun onClickAdd(){
        btn_add.setOnClickListener {
            val intent = Intent(this,AddActivity::class.java)
            startActivity(intent)
        }
    }

    inner class ContactAdapter(dataList: ArrayList<ModelData>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.cell_item,parent,false)
            return ContactViewhold(itemView)
        }

        override fun getItemCount(): Int {
            return dataList.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is ContactViewhold){
                holder.itemView.txt_name.text= dataList[position].name
                holder.itemView.txt_number_phone.text=dataList[position].numberphone
            }
            holder.itemView.setOnClickListener {
                callIntent(dataList[position].id)
            }
        }
        inner class ContactViewhold(itemView: View):RecyclerView.ViewHolder(itemView)

    }
    fun callIntent(id: Int){
        val intentInfo = Intent(this,InfoActivity::class.java)
        intentInfo.putExtra("idInfo",id)
        startActivity(intentInfo)
    }

}
