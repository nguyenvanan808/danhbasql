package com.netfin.danhbab2

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import androidx.core.content.contentValuesOf

class SQLlite(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null,1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE IF NOT EXISTS $TABLE_NAME(ID INTEGER PRIMARY KEY AUTOINCREMENT , NAME TEXT,NUMBERPHONE TEXT,SEX TEXT)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }
    fun insertData(name: String, numberPhone: String,sex: String): Boolean{
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_NAME,name)
        cv.put(COL_PHONE,numberPhone)
        cv.put(COL_SEX,sex)
        val res = db.insert(TABLE_NAME,null,cv)
        return !res.equals(-1)
    }
    fun getAllData(): Cursor {

        val db = this.writableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)
    }

    fun getData(id: String): Cursor {
        val db = this.writableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME WHERE ID=? ", arrayOf(id), null)
    }
    fun daleteData(id: String): Int? {
        val db = this.writableDatabase
        return db.delete(TABLE_NAME, "ID =? ", arrayOf(id))
    }
    fun updateData(id: String, name: String, numberPhone: String, sex: String): Boolean? {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_ID, id)
        cv.put(COL_NAME, name)
        cv.put(COL_PHONE, numberPhone)
        cv.put(COL_SEX, sex)
        db.update(TABLE_NAME, cv, "ID=?", arrayOf(id))
        return true
    }


    companion object{
        val DATABASE_NAME = "danhba.db"
        val TABLE_NAME ="danhba_table"
        val COL_ID = "ID"
        val COL_NAME = "NAME"
        val COL_PHONE = "NUMBERPHONE"
        val COL_SEX = "SEX"

    }
}