package com.netfin.danhbab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.android.synthetic.main.activity_update.*

class UpdateActivity : AppCompatActivity() {
    lateinit var mySQLlite: SQLlite
    var contactInfo: ArrayList<String> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)
        contactInfo = intent.getStringArrayListExtra("idInfo")
        mySQLlite = SQLlite(this)
        edt_name_update.setText(contactInfo[1])
        edt_numberphone_update.setText(contactInfo[2])
        edt_sex_update.setText(contactInfo[3])
        btn_save_update.setOnClickListener {
            updateData()
        }
    }
    fun updateData() {
            val id = contactInfo[0]
            val name = edt_name_update.text.toString().trim()
            val numberphone = edt_numberphone_update.text.toString().trim()
            val sex = edt_sex_update.text.toString().trim()

            if (TextUtils.isEmpty(name)) {
                edt_name_update.error = "Enter name"
                return
            }

            if (TextUtils.isEmpty(numberphone)) {
                edt_numberphone_update.error = "Enter Number Phone"
                return
            }
            if (TextUtils.isEmpty(sex)) {
                edt_sex_update.error = "Enter Sex"
                return
            }

            val isUpdated = mySQLlite.updateData(id, name, numberphone, sex)
            if (isUpdated == true) {
                Toast.makeText(applicationContext, "Data updated ", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(applicationContext, "Data could not be updated ", Toast.LENGTH_SHORT).show()

            }

    }
}
